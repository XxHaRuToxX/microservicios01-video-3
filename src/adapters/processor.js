const { service } =require('../services');
const { InternalError } = require('../settings');

const { queueView } = require('./index');

console.log("LLego aqui");
queueView.process(async(job, done)=>{
    try {
        const { id } = job.data;
        console.log(id)
        let {statusCode, data, message} = await service({id});
        // user = {personal, preference}
        done(null, { statusCode, data, message}); 
    } catch (error) {
        console.log({step: 'adapter Adapters', error: error.toString()})
        done(null, { statusCode: 500, message: InternalError});
    }
})